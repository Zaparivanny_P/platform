#ifndef MODBUSDATAUNIT_H
#define MODBUSDATAUNIT_H

#include <QObject>
#include <QVector>


class ModbusDataUnit
{
public:
    enum RegisterType {
        Invalid,
        DiscreteInputs,
        Coils,
        InputRegisters,
        HoldingRegisters
    };

    ModbusDataUnit() = default;

    explicit ModbusDataUnit(RegisterType type)
        : ModbusDataUnit(type, 0, 0)
    {}

    ModbusDataUnit(RegisterType type, int newStartAddress, quint16 newValueCount)
        : ModbusDataUnit(type, newStartAddress, QVector<quint16>(newValueCount))
    {}

    ModbusDataUnit(RegisterType type, int newStartAddress, const QVector<quint16> &newValues)
        : m_type(type)
        , m_startAddress(newStartAddress)
        , m_values(newValues)
        , m_valueCount(newValues.size())
    {}

    RegisterType registerType() const { return m_type; }
    void setRegisterType(RegisterType type) { m_type = type; }

    inline int startAddress() const { return m_startAddress; }
    inline void setStartAddress(int newAddress) { m_startAddress = newAddress; }

    inline QVector<quint16> values() const { return m_values; }
    inline void setValues(const QVector<quint16> &newValues)
    {
        m_values = newValues;
        m_valueCount = newValues.size();
    }

    inline uint valueCount() const { return m_valueCount; }
    inline void setValueCount(uint newCount) { m_valueCount = newCount; }

    inline void setValue(int index, quint16 newValue)
    {
        if (m_values.isEmpty() || index >= m_values.size())
            return;
        m_values[index] = newValue;
    }
    inline quint16 value(int index) const { return m_values.value(index); }

    bool isValid() const { return m_type != Invalid && m_startAddress != -1; }

private:
    RegisterType m_type = Invalid;
    int m_startAddress = -1;
    QVector<quint16> m_values;
    uint m_valueCount = 0;
};

#endif // MODBUSDATAUNIT_H
