#ifndef MODBUSREPLY_H
#define MODBUSREPLY_H

#include <QObject>
#include "modbuspdu.h"
#include "modbusdataunit.h"

class ModbusReplyPrivate;

class ModbusReply : public QObject
{
    Q_OBJECT
    Q_DECLARE_PRIVATE(ModbusReply)

public:
    
    enum Error {
        NoError,
        ReadError,
        WriteError,
        ConnectionError,
        ConfigurationError,
        TimeoutError,
        ProtocolError,
        ReplyAbortedError,
        UnknownError
    };
    
    enum ReplyType {
        Raw,
        Common
    };
    Q_ENUM(ReplyType)

    ModbusReply(ReplyType type, int serverAddress, QObject *parent = nullptr);

    ReplyType type() const;
    int serverAddress() const;

    bool isFinished() const;

    ModbusDataUnit result() const;
    ModbusResponse rawResult() const;

    QString errorString() const;
    ModbusReply::Error error() const;

    void setResult(const ModbusDataUnit &unit);
    void setRawResult(const ModbusResponse &unit);

    void setFinished(bool isFinished);
    void setError(ModbusReply::Error error, const QString &errorText);
private:
    ModbusReplyPrivate *d_ptr;

signals:
    void finished();
    void errorOccurred(ModbusReply::Error error);
};

#endif // MODBUSREPLY_H
