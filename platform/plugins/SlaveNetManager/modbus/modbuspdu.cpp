#include "modbuspdu.h"

#include <QDataStream>
#include <QDebug>
#include <QIODevice>

namespace EncapsulatedInterfaceTransport {

    enum SubFunctionCode {
        CanOpenGeneralReference = 0x0D,
        ReadDeviceIdentification = 0x0E
    };

}

namespace Diagnostics {

    enum SubFunctionCode {
        ReturnQueryData = 0x0000,
        RestartCommunicationsOption = 0x0001,
        ReturnDiagnosticRegister = 0x0002,
        ChangeAsciiInputDelimiter = 0x0003,
        ForceListenOnlyMode = 0x0004,
        ClearCountersAndDiagnosticRegister = 0x000a,
        ReturnBusMessageCount = 0x000b,
        ReturnBusCommunicationErrorCount = 0x000c,  // CRC error counter
        ReturnBusExceptionErrorCount = 0x000d,
        ReturnServerMessageCount = 0x000e,
        ReturnServerNoResponseCount = 0x000f,
        ReturnServerNAKCount = 0x0010,
        ReturnServerBusyCount = 0x0011,
        ReturnBusCharacterOverrunCount = 0x0012,
        ClearOverrunCounterAndFlag = 0x0014
    };

}


using ReqSizeCalc = QHash<quint8, ModbusRequest::CalcFuncPtr>;
Q_GLOBAL_STATIC(ReqSizeCalc, requestSizeCalculators);

using ResSizeCalc = QHash<quint8, ModbusResponse::CalcFuncPtr>;
Q_GLOBAL_STATIC(ResSizeCalc, responseSizeCalculators);


namespace Private {

enum struct Type {
    Request,
    Response
};

static int minimumDataSize(const ModbusPdu &pdu, Type type)
{
    if (pdu.isException())
        return 1;

    switch (pdu.functionCode()) {
    case ModbusPdu::ReadCoils:
    case ModbusPdu::ReadDiscreteInputs:
        return type == Type::Request ? 4 : 2;
    case ModbusPdu::WriteSingleCoil:
    case ModbusPdu::WriteSingleRegister:
        return 4;
    case ModbusPdu::ReadHoldingRegisters:
    case ModbusPdu::ReadInputRegisters:
        return type == Type::Request ? 4 : 3;
    case ModbusPdu::ReadExceptionStatus:
        return type == Type::Request ? 0 : 1;
    case ModbusPdu::Diagnostics:
        return 4;
    case ModbusPdu::GetCommEventCounter:
        return type == Type::Request ? 0 : 4;
    case ModbusPdu::GetCommEventLog:
        return type == Type::Request ? 0 : 8;
    case ModbusPdu::WriteMultipleCoils:
        return type == Type::Request ? 6 : 4;
    case ModbusPdu::WriteMultipleRegisters:
        return type == Type::Request ? 7 : 4;
    case ModbusPdu::ReportServerId:
        return type == Type::Request ? 0 : 3;
    case ModbusPdu::ReadFileRecord:
        return type == Type::Request ? 8 : 5;
    case ModbusPdu::WriteFileRecord:
        return 10;
    case ModbusPdu::MaskWriteRegister:
        return 6;
    case ModbusPdu::ReadWriteMultipleRegisters:
        return type == Type::Request ? 11 : 3;
    case ModbusPdu::ReadFifoQueue:
        return type == Type::Request ? 2 : 6;
    case ModbusPdu::EncapsulatedInterfaceTransport:
        return 2;
    case ModbusPdu::Invalid:
    case ModbusPdu::UndefinedFunctionCode:
        return -1;
    }
    return -1;
}

static QDataStream &pduFromStream(QDataStream &stream, ModbusPdu &pdu, Type type)
{
    ModbusPdu::FunctionCode code = ModbusPdu::Invalid;
    if (stream.readRawData((char *) (&code), sizeof(quint8)) != sizeof(quint8))
        return stream;
    pdu.setFunctionCode(code);

    auto needsAdditionalRead = [](ModbusPdu &pdu, int size) -> bool {
        if (size < 0)
            pdu.setFunctionCode(ModbusResponse::Invalid);
        if (size <= 0)
            return false;
        return true;
    };

    const bool isResponse = (type == Type::Response);
    int size = isResponse ? ModbusResponse::minimumDataSize(pdu)
                          : ModbusRequest::minimumDataSize(pdu);
    if (!needsAdditionalRead(pdu, size))
        return stream;

    QByteArray data(size, Qt::Uninitialized);
    if (stream.device()->peek(data.data(), data.size()) != size)
        return stream;

    pdu.setData(data);
    size = isResponse ? ModbusResponse::calculateDataSize(pdu)
                      : ModbusRequest::calculateDataSize(pdu);
    if (!needsAdditionalRead(pdu, size))
        return stream;

    if (isResponse && (code == ModbusPdu::EncapsulatedInterfaceTransport)) {
        quint8 meiType;
        pdu.decodeData(&meiType);
        if (meiType == EncapsulatedInterfaceTransport::ReadDeviceIdentification) {
            int left = size, offset = 0;
            while ((left > 0) && (size <= 252)) { // The maximum PDU data size is 252 bytes.
                data.resize(size);
                const int read = stream.readRawData(data.data() + offset, size - offset);
                if ((read < 0) || (read != (size - offset))) {
                    size = 255; // bogus size
                    stream.setStatus(QDataStream::ReadCorruptData);
                    break; // error reading, bail, reset further down
                }
                offset += read;
                left = ModbusResponse::calculateDataSize(ModbusResponse(code, data)) - offset;
                size += left;
            }
            if ((stream.status() == QDataStream::Ok) && (size <= 252)) {
                pdu.setData(data);
                return stream; // early return to avoid second read
            }
        } else {
            data.resize(stream.device()->size() - 1); // One byte for the function code.
        }
    } else if (pdu.functionCode() == ModbusPdu::Diagnostics) {
        quint16 subCode;
        pdu.decodeData(&subCode);
        if (subCode == Diagnostics::ReturnQueryData)
            data.resize(stream.device()->size() - 1); // One byte for the function code.
    }

    // reset what we have so far, next read might fail as well
    pdu.setData(QByteArray());
    pdu.setFunctionCode(ModbusPdu::Invalid);

    if (data.size() <= 252) { // The maximum PDU data size is 252 bytes.
        data.resize(size);
        if (stream.readRawData(data.data(), data.size()) == size) {
            pdu.setData(data);
            pdu.setFunctionCode(code);
        }
    }
    return stream;
}

}   // namespace Private

QDebug operator<<(QDebug debug, const ModbusPdu &pdu)
{
    QDebugStateSaver _(debug);
    debug.nospace().noquote() << "0x" << hex << qSetFieldWidth(2) << qSetPadChar('0')
        << (pdu.isException() ? pdu.functionCode() | ModbusPdu::ExceptionByte : pdu.functionCode())
        << qSetFieldWidth(0) << pdu.data().toHex();
    return debug;
}

QDataStream &operator<<(QDataStream &stream, const ModbusPdu &pdu)
{
    if (pdu.isException())
        stream << static_cast<quint8> (pdu.functionCode() | ModbusPdu::ExceptionByte);
    else
        stream << static_cast<quint8> (pdu.functionCode());
    if (!pdu.data().isEmpty())
        stream.writeRawData(pdu.data().constData(), pdu.data().size());

    return stream;
}

int ModbusRequest::minimumDataSize(const ModbusRequest &request)
{
    return Private::minimumDataSize(request, Private::Type::Request);
}

int ModbusRequest::calculateDataSize(const ModbusRequest &request)
{
    if (requestSizeCalculators.exists()) {
        if (auto ptr = requestSizeCalculators()->value(request.functionCode(), nullptr))
            return ptr(request);
    }

    if (request.isException())
        return 1;

    int size = -1;
    int minimum = Private::minimumDataSize(request, Private::Type::Request);
    if (minimum < 0)
        return size;

    switch (request.functionCode()) {
    case ModbusPdu::WriteMultipleCoils:
        minimum -= 1; // first payload payload byte
        if (request.dataSize() >= minimum)
            size = minimum + request.data()[minimum - 1] /*byte count*/;
        break;
    case ModbusPdu::WriteMultipleRegisters:
    case ModbusPdu::ReadWriteMultipleRegisters:
        minimum -= 2; // first 2 payload payload bytes
        if (request.dataSize() >= minimum)
            size = minimum + request.data()[minimum - 1] /*byte count*/;
        break;
    case ModbusPdu::ReadFileRecord:
    case ModbusPdu::WriteFileRecord:
        if (request.dataSize() >= 1)
            size = 1 /*byte count*/ + request.data()[0] /*actual bytes*/;
        break;
    case ModbusPdu::EncapsulatedInterfaceTransport: {
        if (request.dataSize() < minimum)
            break;  // can't calculate, let's return -1 to indicate error
        quint8 meiType;
        request.decodeData(&meiType);
        // ReadDeviceIdentification -> 3 == MEI type + Read device ID + Object Id
        size = (meiType == EncapsulatedInterfaceTransport::ReadDeviceIdentification) ? 3 : minimum;
    }   break;
    default:
        size = minimum;
        break;
    }
    return size;
}

void ModbusRequest::registerDataSizeCalculator(FunctionCode fc, CalcFuncPtr calculator)
{
    requestSizeCalculators()->insert(fc, calculator);
}


QDataStream &operator>>(QDataStream &stream, ModbusRequest &pdu)
{
    return Private::pduFromStream(stream, pdu, Private::Type::Request);
}

int ModbusResponse::minimumDataSize(const ModbusResponse &response)
{
    return Private::minimumDataSize(response, Private::Type::Response);
}

int ModbusResponse::calculateDataSize(const ModbusResponse &response)
{
    if (responseSizeCalculators.exists()) {
        if (auto ptr = responseSizeCalculators()->value(response.functionCode(), nullptr))
            return ptr(response);
    }

    if (response.isException())
        return 1;

    int size = -1;
    int minimum = Private::minimumDataSize(response, Private::Type::Response);
    if (minimum < 0)
        return size;

    switch (response.functionCode()) {
    case ModbusResponse::ReadCoils:
    case ModbusResponse::ReadDiscreteInputs:
    case ModbusResponse::ReadHoldingRegisters:
    case ModbusResponse::ReadInputRegisters:
    case ModbusResponse::GetCommEventLog:
    case ModbusResponse::ReadFileRecord:
    case ModbusResponse::WriteFileRecord:
    case ModbusResponse::ReadWriteMultipleRegisters:
    case ModbusResponse::ReportServerId:
        if (response.dataSize() >= 1)
            size = 1 /*byte count*/ + response.data()[0] /*actual bytes*/;
        break;
    case ModbusResponse::ReadFifoQueue: {
        if (response.dataSize() >= 2) {
            quint16 rawSize;
            response.decodeData(&rawSize);
            size = rawSize + 2; // 2 bytes size info
        }
    }   break;
    case ModbusPdu::EncapsulatedInterfaceTransport: {
        if (response.dataSize() < minimum)
            break;  // can't calculate, let's return -1 to indicate error

        quint8 meiType;
        response.decodeData(&meiType);

        // update size, header 6 bytes: mei type + read device id + conformity level + more follows
        //                              + next object id + number of object
        // response data part  2 bytes: + object id + object size of the first object -> 8
        size = (meiType == EncapsulatedInterfaceTransport::ReadDeviceIdentification) ? 8 : minimum;
        if (meiType != EncapsulatedInterfaceTransport::ReadDeviceIdentification
            || response.dataSize() < size) {
            break; // TODO: calculate CanOpenGeneralReference instead of break
        }

        const QByteArray data = response.data();
        quint8 numOfObjects = quint8(data[5]);
        quint8 objectSize = quint8(data[7]);

        // 6 byte header size + (2 * n bytes fixed per object) + first object size
        size = 6 + (2 * numOfObjects) + objectSize;
        if ((numOfObjects == 1) || (data.size() < size))
            break;

        // header + object id + object size + second object id (9 bytes) + first object size
        int nextSizeField = 9 + objectSize;
        for (int i = 1; i < numOfObjects; ++i) {
            if (data.size() <= nextSizeField)
                break;
            objectSize = data[nextSizeField];
            size += objectSize;
            nextSizeField += objectSize + 2; // object size + object id field + object size field
        }
    }   break;
    default:
        size = minimum;
        break;
    }
    return size;
}

void ModbusResponse::registerDataSizeCalculator(FunctionCode fc, CalcFuncPtr calculator)
{
    responseSizeCalculators()->insert(fc, calculator);
}

QDataStream &operator>>(QDataStream &stream, ModbusResponse &pdu)
{
    return Private::pduFromStream(stream, pdu, Private::Type::Response);
}
