#include "modbusreply.h"
#include <QObject>
#include <QDebug>

class ModbusReplyPrivate
{
    Q_DECLARE_PUBLIC(ModbusReply)

public:
    ModbusDataUnit m_unit;
    int m_serverAddress = 1;
    bool m_finished = false;
    ModbusReply::Error m_error = ModbusReply::NoError;
    QString m_errorText;
    ModbusResponse m_response;
    ModbusReply::ReplyType m_type;
    ModbusReply *q_ptr;
protected:
    ModbusReplyPrivate(ModbusReply *q) : q_ptr(q) {}
};

ModbusReply::ModbusReply(ReplyType type, int serverAddress, QObject *parent)
    : d_ptr(new ModbusReplyPrivate(this)), QObject(parent)
{
    Q_D(ModbusReply);
    d->m_type = type;
    d->m_serverAddress = serverAddress;
}

bool ModbusReply::isFinished() const
{
    Q_D(const ModbusReply);
    return d->m_finished;
}

void ModbusReply::setFinished(bool isFinished)
{
    Q_D(ModbusReply);
    d->m_finished = isFinished;
    if (isFinished)
        emit finished();
}

ModbusDataUnit ModbusReply::result() const
{
    Q_D(const ModbusReply);
    if (type() == ModbusReply::Common)
        return d->m_unit;
    return ModbusDataUnit();
}

void ModbusReply::setResult(const ModbusDataUnit &unit)
{
    Q_D(ModbusReply);
    d->m_unit = unit;
}

int ModbusReply::serverAddress() const
{
    Q_D(const ModbusReply);
    return d->m_serverAddress;
}

ModbusReply::Error ModbusReply::error() const
{
    Q_D(const ModbusReply);
    return d->m_error;
}

void ModbusReply::setError(ModbusReply::Error error, const QString &errorText)
{
    Q_D(ModbusReply);
    d->m_error = error;
    d->m_errorText = errorText;
    emit errorOccurred(error);
    setFinished(true);
}

QString ModbusReply::errorString() const
{
    Q_D(const ModbusReply);
    return d->m_errorText;
}

ModbusReply::ReplyType ModbusReply::type() const
{
    Q_D(const ModbusReply);
    return d->m_type;
}

ModbusResponse ModbusReply::rawResult() const
{
    Q_D(const ModbusReply);
    return d->m_response;
}

void ModbusReply::setRawResult(const ModbusResponse &response)
{
    Q_D(ModbusReply);
    d->m_response = response;
}
