#ifndef MODBUSCLIENT_H
#define MODBUSCLIENT_H

#include <QObject>
#include "modbuspdu.h"
#include "modbusdataunit.h"
#include "modbusreply.h"
#include "../net/netslaveclient.h"
#include "../net/netprotocol.h"

class ModbusClient : public QObject
{
    Q_OBJECT
public:
    explicit ModbusClient(QObject *parent = 0);
    ~ModbusClient();
    
    void setNetClient(NetSlaveClient *client);

    ModbusReply *sendReadRequest(const ModbusDataUnit &read, int serverAddress);
    ModbusReply *sendWriteRequest(const ModbusDataUnit &write, int serverAddress);
    ModbusReply *sendReadWriteRequest(const ModbusDataUnit &read, const ModbusDataUnit &write,
                                       int serverAddress);
    ModbusReply *sendRawRequest(const ModbusRequest &request, int serverAddress);

    int timeout() const;
    void setTimeout(int newTimeout);

    int numberOfRetries() const;
    void setNumberOfRetries(int number);
private:
    bool collateBytes(const ModbusPdu &response,
                        ModbusDataUnit::RegisterType type, ModbusDataUnit *data);
    bool collateMultipleValues(const ModbusPdu &response,
                                          ModbusDataUnit::RegisterType type, ModbusDataUnit *data);
private:
    NetSlaveClient* m_client = nullptr;
    NetProtocol *m_protocol;
signals:
    void timeoutChanged(int newTimeout);
public slots:
};

#endif // MODBUSCLIENT_H
