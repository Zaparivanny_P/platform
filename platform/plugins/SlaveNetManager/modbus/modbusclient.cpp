#include "modbusclient.h"
#include <QDebug>

#include "../net/netclientreply.h"


ModbusClient::ModbusClient(QObject *parent) : QObject(parent)
{
    
}

ModbusClient::~ModbusClient()
{
    
}

void ModbusClient::setNetClient(NetSlaveClient *client)
{
    m_client = client;
    
    NetConnection *connection = new NetConnection;
    
    connection->setBaudrate(QSerialPort::Baud115200);
    connection->setDataBits(QSerialPort::Data8);
    connection->setFlowControl(QSerialPort::NoFlowControl);
    connection->setParity(QSerialPort::NoParity);
    connection->setStopBits(QSerialPort::OneStop);
    
    
    m_protocol = new NetProtocol(m_client, connection);
    m_protocol->setPortName("COM3");
}

ModbusReply *ModbusClient::sendReadRequest(const ModbusDataUnit &read, int serverAddress)
{
    ModbusReply * mreply = new ModbusReply(ModbusReply::Common, serverAddress);
    Q_ASSERT_X(m_client != nullptr, "ModbusClient::sendReadRequest", "");
    
    QByteArray data;
    QDataStream out(&data, QIODevice::WriteOnly);
    
    quint8 cmd;
    switch(read.registerType())
    {
    case ModbusDataUnit::DiscreteInputs:
        cmd = 1;
        break;
    case ModbusDataUnit::Coils:
        cmd = 2;
        break;
    case ModbusDataUnit::InputRegisters:
        cmd = 4;
        break;
    case ModbusDataUnit::HoldingRegisters:
        cmd = 3;
        break;
    }
    out << (quint8)cmd;
    out << (quint16)read.startAddress();
    out << (quint16)read.valueCount();
    
    
    NetClientReply *reply = m_protocol->sendModusPdu(serverAddress, data);
    
    connect(reply, &NetClientReply::finished, [this, reply, mreply, serverAddress, read](){
        
        if(reply->error1() || reply->error2())
        {
            mreply->setError(ModbusReply::TimeoutError, "time out TODO");
        }
        else
        {
            ModbusDataUnit unit;
            unit.setStartAddress(read.startAddress());
            
            QByteArray d = reply->data();
            ModbusResponse response((ModbusResponse::FunctionCode)d.at(0), d.mid(1));
            mreply->setRawResult(response);
            bool res = collateBytes(response, read.registerType(), &unit);
            //Q_ASSERT_X(res, "[ModbusClient::sendReadRequest]", "");
            mreply->setResult(unit);
            mreply->setFinished(true);
        }
        reply->deleteLater();
        
    });
    return mreply;
}

ModbusReply *ModbusClient::sendWriteRequest(const ModbusDataUnit &write, int serverAddress)
{
    ModbusReply * mreply = new ModbusReply(ModbusReply::Raw, serverAddress);
    Q_ASSERT_X(m_client != nullptr, "ModbusClient::sendWriteRequest", "");
    
    
    QByteArray data;
    QDataStream out(&data, QIODevice::WriteOnly);
    out << (quint8)16;
    out << (quint16)write.startAddress();
    out << (quint16)write.valueCount();
    out << (quint8)(write.valueCount() * 2);
    auto vals = write.values();
    for(int i = 0; i < vals.size(); i++)
    {
        out << (quint16)vals[i];
    }
    qDebug() << "[ModbusClient::sendWriteRequest] data" << data;
    NetClientReply *reply = m_protocol->sendModusPdu(serverAddress, data);
    
    connect(reply, &NetClientReply::finished, [this, reply, mreply, serverAddress, write](){
        qDebug() << "[ModbusClient::sendRawRequest]" << reply->data();
        if(reply->error1() || reply->error2())
        {
            mreply->setError(ModbusReply::TimeoutError, "time out TODO");
        }
        else
        {
            ModbusDataUnit unit;
    
            QByteArray d = reply->data();
            ModbusResponse response((ModbusResponse::FunctionCode)d.at(0), d.mid(1));
            
            bool res = collateMultipleValues(response, write.registerType(), &unit);
            //Q_ASSERT_X(res, "[ModbusClient::sendWriteRequest]", "");
            mreply->setRawResult(response);
            mreply->setResult(unit);
            mreply->setFinished(true);
        }
        reply->deleteLater();
        
    });
    return mreply;
}

ModbusReply *ModbusClient::sendReadWriteRequest(const ModbusDataUnit &read, 
                                                const ModbusDataUnit &write, 
                                                int serverAddress)
{
    Q_UNUSED(read);
    Q_UNUSED(write);
    Q_UNUSED(serverAddress);
    return nullptr;
}

ModbusReply *ModbusClient::sendRawRequest(const ModbusRequest &request, int serverAddress)
{
    ModbusReply * mreply = new ModbusReply(ModbusReply::Raw, serverAddress);
    Q_ASSERT_X(m_client != nullptr, "ModbusClient::sendRawRequest", "");
    qDebug() << "[ModbusClient::sendRawRequest]" << request.functionCode() << request.data() << serverAddress;
    QByteArray data = request.data();
    data.prepend(request.functionCode());
    NetClientReply *reply = m_protocol->sendModusPdu(serverAddress, data);
    
    connect(reply, &NetClientReply::finished, [this, reply, mreply](){
        
        if(reply->error1() || reply->error2())
        {
            mreply->setError(ModbusReply::TimeoutError, "time out TODO");
        }
        else
        {
            ModbusResponse response;
            
            response.setData(reply->data());
            mreply->setRawResult(response);
            mreply->setFinished(true);
        }
        reply->deleteLater();
    });
    return mreply;
}

int ModbusClient::timeout() const
{
    
}

void ModbusClient::setTimeout(int newTimeout)
{
    
}

int ModbusClient::numberOfRetries() const
{
    
}

void ModbusClient::setNumberOfRetries(int number)
{
    
}

bool ModbusClient::collateBytes(const ModbusPdu &response, ModbusDataUnit::RegisterType type, ModbusDataUnit *data)
{
    if (response.dataSize() < ModbusResponse::minimumDataSize(response))
        return false;

    // byte count needs to match available bytes
    const quint8 byteCount = response.data()[0];
    if ((response.dataSize() - 1) != byteCount)
        return false;

    // byte count needs to be odd to match full registers
    if (byteCount % 2 != 0)
        return false;

    if (data) 
    {
        QDataStream stream(response.data().remove(0, 1));

        QVector<quint16> values;
        const quint8 itemCount = byteCount / 2;
        for (int i = 0; i < itemCount; i++) 
        {
            quint16 tmp;
            stream >> tmp;
            values.append(tmp);
        }
        data->setValues(values);
        data->setRegisterType(type);
    }
    return true;
}

bool ModbusClient::collateMultipleValues(const ModbusPdu &response, ModbusDataUnit::RegisterType type, ModbusDataUnit *data)
{
    if (response.dataSize() != ModbusResponse::minimumDataSize(response))
        return false;

    quint16 address, count;
    response.decodeData(&address, &count);

    // number of registers to write is 1-123 per request
    if ((type == ModbusDataUnit::HoldingRegisters) && (count < 1 || count > 123))
        return false;

    if (data) 
    {
        data->setValueCount(count);
        data->setRegisterType(type);
        data->setStartAddress(address);
    }
    return true;
}
