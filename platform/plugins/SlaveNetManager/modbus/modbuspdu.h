#ifndef MODBUSPDU_H
#define MODBUSPDU_H

#include <QObject>
#include <QIODevice>
#include <QDataStream>

class ModbusPdu
{
public:
    enum ExceptionCode {
        IllegalFunction = 0x01,
        IllegalDataAddress = 0x02,
        IllegalDataValue = 0x03,
        ServerDeviceFailure = 0x04,
        Acknowledge = 0x05,
        ServerDeviceBusy = 0x06,
        NegativeAcknowledge = 0x07,
        MemoryParityError = 0x08,
        GatewayPathUnavailable = 0x0A,
        GatewayTargetDeviceFailedToRespond = 0x0B,
        ExtendedException = 0xFF,
    };

    enum FunctionCode {
        Invalid = 0x00,
        ReadCoils = 0x01,
        ReadDiscreteInputs = 0x02,
        ReadHoldingRegisters = 0x03,
        ReadInputRegisters = 0x04,
        WriteSingleCoil = 0x05,
        WriteSingleRegister = 0x06,
        ReadExceptionStatus = 0x07,
        Diagnostics = 0x08,
        GetCommEventCounter = 0x0B,
        GetCommEventLog = 0x0C,
        WriteMultipleCoils = 0x0F,
        WriteMultipleRegisters = 0x10,
        ReportServerId = 0x11,
        ReadFileRecord = 0x14,
        WriteFileRecord = 0x15,
        MaskWriteRegister = 0x16,
        ReadWriteMultipleRegisters = 0x17,
        ReadFifoQueue = 0x18,
        EncapsulatedInterfaceTransport = 0x2B,
        UndefinedFunctionCode = 0x100
    };

    ModbusPdu() = default;
    virtual ~ModbusPdu() = default;

    bool isValid() const {
        return (m_code >= ReadCoils && m_code < UndefinedFunctionCode)
                && (m_data.size() < 253);
    }

    static const quint8 ExceptionByte = 0x80;
    ExceptionCode exceptionCode() const {
        if (!m_data.size() || !isException())
            return ExtendedException;
        return static_cast<ExceptionCode>(m_data.at(0));
    }
    bool isException() const { return m_code & ExceptionByte; }

    qint16 size() const { return dataSize() + 1; }
    qint16 dataSize() const { return m_data.size(); }

    FunctionCode functionCode() const {
        return FunctionCode(quint8(m_code) &~ ExceptionByte);
    }
    virtual void setFunctionCode(FunctionCode code) { m_code = code; }

    QByteArray data() const { return m_data; }
    void setData(const QByteArray &newData) { m_data = newData; }

    template <typename ... Args> void encodeData(Args ... newData) {
        encode(std::forward<Args>(newData)...);
    }

    template <typename ... Args> void decodeData(Args && ... newData) const {
        decode(std::forward<Args>(newData)...);
    }

protected:
    ModbusPdu(FunctionCode code, const QByteArray &newData)
        : m_code(code)
        , m_data(newData)
    {}

    ModbusPdu(const ModbusPdu &) = default;
    ModbusPdu &operator=(const ModbusPdu &) = default;

    template <typename ... Args>
    ModbusPdu(FunctionCode code, Args ... newData)
        : m_code(code)
    {
        encode(std::forward<Args>(newData)...);
    }

private:
    template <typename T, typename ... Ts> struct IsType { enum { value = false }; };
    template <typename T, typename T1, typename ... Ts> struct IsType<T, T1, Ts...> {
        enum { value = std::is_same<T, T1>::value || IsType<T, Ts...>::value };
    };

    template <typename T> void encode(QDataStream *stream, const T &t) {
        static_assert(std::is_pod<T>::value, "Only POD types supported.");
        static_assert(IsType<T, quint8, quint16>::value, "Only quint8 and quint16 supported.");
        (*stream) << t;
    }
    template <typename T> void decode(QDataStream *stream, T &t) const {
        static_assert(std::is_pod<T>::value, "Only POD types supported.");
        static_assert(IsType<T, quint8 *, quint16 *>::value, "Only quint8* and quint16* supported.");
        (*stream) >> *t;
    }
    template <typename T> void encode(QDataStream *stream, const QVector<T> &vector) {
        static_assert(std::is_pod<T>::value, "Only POD types supported.");
        static_assert(IsType<T, quint8, quint16>::value, "Only quint8 and quint16 supported.");
        for (int i = 0; i < vector.count(); ++i)
            (*stream) << vector[i];
    }

    template<typename ... Args> void encode(Args ... newData) {
        m_data.clear();
        Q_CONSTEXPR quint32 argCount = sizeof...(Args);
        if (argCount > 0) {
            QDataStream stream(&m_data, QIODevice::WriteOnly);
            char tmp[argCount] = { (encode(&stream, newData), void(), '0')... };
            Q_UNUSED(tmp)
        }
    }
    template<typename ... Args> void decode(Args ... newData) const {
        Q_CONSTEXPR quint32 argCount = sizeof...(Args);
        if (argCount > 0 && !m_data.isEmpty()) {
            QDataStream stream(m_data);
            char tmp[argCount] = { (decode(&stream, newData), void(), '0')... };
            Q_UNUSED(tmp)
        }
    }

private:
    FunctionCode m_code = Invalid;
    QByteArray m_data;
    friend class QModbusSerialAdu;
};
QDebug operator<<(QDebug debug, const ModbusPdu &pdu);
QDataStream &operator<<(QDataStream &stream, const ModbusPdu &pdu);

class ModbusRequest : public ModbusPdu
{
public:
    ModbusRequest() = default;
    ModbusRequest(const ModbusPdu &pdu)
        : ModbusPdu(pdu)
    {}

    explicit ModbusRequest(FunctionCode code, const QByteArray &newData = QByteArray())
        : ModbusPdu(code, newData)
    {}

    static int minimumDataSize(const ModbusRequest &pdu);
    static int calculateDataSize(const ModbusRequest &pdu);

    using CalcFuncPtr = decltype(&calculateDataSize);
    static void registerDataSizeCalculator(FunctionCode fc, CalcFuncPtr func);

    template <typename ... Args>
    ModbusRequest(FunctionCode code, Args ... newData)
        : ModbusPdu(code, newData...)
    {}
};
QDataStream &operator>>(QDataStream &stream, ModbusRequest &pdu);

class ModbusResponse : public ModbusPdu
{
public:
    ModbusResponse() = default;
    ModbusResponse(const ModbusPdu &pdu)
        : ModbusPdu(pdu)
    {}

    explicit ModbusResponse(FunctionCode code, const QByteArray &newData = QByteArray())
        : ModbusPdu(code, newData)
    {}

    static int minimumDataSize(const ModbusResponse &pdu);
    static int calculateDataSize(const ModbusResponse &pdu);

    using CalcFuncPtr = decltype(&calculateDataSize);
    static void registerDataSizeCalculator(FunctionCode fc, CalcFuncPtr func);

    template <typename ... Args>
    ModbusResponse(FunctionCode code, Args ... newData)
        : ModbusPdu(code, newData...)
    {}
};

class ModbusExceptionResponse : public ModbusResponse
{
public:
    ModbusExceptionResponse() = default;
    ModbusExceptionResponse(const ModbusPdu &pdu)
        : ModbusResponse(pdu)
    {}
    ModbusExceptionResponse(FunctionCode fc, ExceptionCode ec)
        : ModbusResponse(FunctionCode(quint8(fc) | ExceptionByte), static_cast<quint8> (ec))
    {}

    void setFunctionCode(FunctionCode c) {
        ModbusPdu::setFunctionCode(FunctionCode(quint8(c) | ExceptionByte));
    }
    void setExceptionCode(ExceptionCode ec) { ModbusPdu::encodeData(quint8(ec)); }
};
QDataStream &operator>>(QDataStream &stream, ModbusResponse &pdu);

#endif // MODBUSPDU_H
