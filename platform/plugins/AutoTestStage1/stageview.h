#ifndef STAGEVIEW_H
#define STAGEVIEW_H

#include <QWidget>
#include "../AutoTest/iautoteststage.h"

class StageView : public IAutoTestStage
{
    Q_OBJECT
public:
    explicit StageView(QObject *parent = 0);
    
    virtual QWidget *widget() override;
    virtual QString name() override;
    virtual QIcon icon() override;
signals:
    
public slots:
};

#endif // STAGEVIEW_H
