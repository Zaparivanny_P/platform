#include "autoteststage1.h"
#include "stageview.h"

AutoTestStage1::AutoTestStage1(QObject *parent) : AbstractPlugin(parent)
{
    
}

bool AutoTestStage1::initialize()
{
    addAutoReleasedObject(new StageView());
}
