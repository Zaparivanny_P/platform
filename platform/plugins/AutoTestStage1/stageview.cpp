#include "stageview.h"
#include <QIcon>

StageView::StageView(QObject *parent) : IAutoTestStage(parent)
{
    
}

QWidget *StageView::widget()
{
    QWidget *w1 = new QWidget();
    w1->setStyleSheet("background-color:blue;");
    return w1;
}

QString StageView::name()
{
    return "Этап 1";
}

QIcon StageView::icon()
{
    return QIcon(":/icons/route.png");
}
