#ifndef AUTOTESTSTAGE1_H
#define AUTOTESTSTAGE1_H

#include <QObject>
#include "abstractplugin.h"

class AutoTestStage1 : public AbstractPlugin
{
    Q_OBJECT
public:
    explicit AutoTestStage1(QObject *parent = 0);
    virtual bool initialize() override;
signals:
    
public slots:
    
};

#endif // AUTOTESTSTAGE1_H
