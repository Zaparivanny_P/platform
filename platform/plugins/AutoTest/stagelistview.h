#ifndef STAGELISTVIEW_H
#define STAGELISTVIEW_H

#include <QWidget>
#include <QStackedLayout>

class QListWidget;
class QListWidgetItem;


class StageListView : public QWidget
{
    Q_OBJECT
public:
    explicit StageListView(QWidget *parent = 0);
    void insertTab(int index, QWidget *tab, const QIcon &icon, const QString &label);
    void removeTab(int index);
    int size();
private:
    QStackedLayout *m_modesStack;
    QWidget *m_currentWidget;
    QListWidget *m_list;
signals:
    void currentChanged(int index);
public slots:
    void setCurrentIndex(int index);
    void itemChanged(QListWidgetItem*item);
};

#endif // STAGELISTVIEW_H
