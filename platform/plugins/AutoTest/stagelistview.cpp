#include "stagelistview.h"

#include <QPushButton>
#include <QVBoxLayout>
#include <QLabel>
#include <QIcon>
#include <QStyle>
#include <QApplication>
#include <QStackedWidget>
#include <QListWidget>
#include <QDebug>

StageListView::StageListView(QWidget *parent) : QWidget(parent)
{
    m_list = new QListWidget(parent);
    m_list->setMaximumWidth(70);
    m_modesStack = new QStackedLayout(parent);
    m_currentWidget = new QWidget;
    m_currentWidget->setLayout(m_modesStack);
    
    //m_list->setViewMode(QListView::IconMode);
    m_list->setFlow(QListView::TopToBottom);
    m_list->setFocusPolicy(Qt::NoFocus); 
    m_list->setMovement(QListView::Static);
    m_list->setResizeMode(QListView::Adjust);
    
    m_list->setWrapping(true);
    m_list->setTextElideMode(Qt::ElideRight);
    m_list->setUniformItemSizes(true);
    m_list->setFrameShape(QFrame::NoFrame);
    
    connect(m_list, SIGNAL(itemClicked(QListWidgetItem*)), SLOT(itemChanged(QListWidgetItem*)));
    
    
    QLayout *vlay = new QHBoxLayout();
    
    vlay->addWidget(m_currentWidget);
    vlay->addWidget(m_list);
    
    vlay->setMargin(0);
    vlay->setSpacing(0);
    
    setLayout(vlay);
}

void StageListView::insertTab(int index, QWidget *tab, const QIcon &icon, const QString &label)
{
    QListWidgetItem *item = new QListWidgetItem(icon, label);
    item->setSizeHint(QSize(70, 70));
    m_list->insertItem(index, item);
    int row = m_list->row(item);
    
    m_modesStack->insertWidget(row, tab);
}

void StageListView::removeTab(int index)
{
    m_modesStack->removeWidget(m_modesStack->widget(index));
}

int StageListView::size()
{
    return m_modesStack->count();
}

void StageListView::setCurrentIndex(int index)
{
    m_modesStack->setCurrentIndex(index);
}

void StageListView::itemChanged(QListWidgetItem *item)
{
    int index = m_list->row(item);
    m_modesStack->setCurrentIndex(index);
}
