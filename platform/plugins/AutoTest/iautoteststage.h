#ifndef IAUTOTESTSTAGE_H
#define IAUTOTESTSTAGE_H

#include <QObject>

class IAutoTestStage : public QObject
{
    Q_OBJECT
public:
    explicit IAutoTestStage(QObject *parent = 0);
    virtual QWidget *widget() = 0;
    virtual QString name() = 0;
    virtual QIcon icon() = 0;
signals:
    
public slots:
};

#endif // IAUTOTESTSTAGE_H
