#include "controlpanel.h"
#include <QLayout>
#include <QPushButton>
#include <QIcon>
#include <QToolButton>

ControlPanel::ControlPanel(QWidget *parent) : QWidget(parent)
{
    QLayout *vlay = new QHBoxLayout();
    
    QToolButton *btn = new QToolButton();
    btn->setIcon(QIcon(":/icons/play-button.png"));
    btn->setIconSize(QSize(30, 30));
    btn->setText("Старт");
    btn->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
    btn->setMinimumSize(50, 50);
    btn->setMaximumSize(50, 50);
    
    
    QToolButton *btn2 = new QToolButton();
    btn2->setIcon(QIcon(":/icons/stop.png"));
    btn2->setIconSize(QSize(30, 30));
    btn2->setText("Старт");
    btn2->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
    btn2->setMinimumSize(50, 50);
    btn2->setMaximumSize(50, 50);
    
    vlay->addWidget(btn);
    vlay->addWidget(btn2);
    //vlay->setSpacing(0);
    vlay->setMargin(0);
    vlay->setAlignment(Qt::AlignLeft);
    setLayout(vlay);
}
