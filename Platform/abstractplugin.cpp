#include "abstractplugin.h"
#include "pluginmanager.h"

AbstractPlugin::AbstractPlugin(QObject *parent) : QObject(parent)
{

}

void AbstractPlugin::addAutoReleasedObject(QObject *obj)
{
    PluginManager::addObject(obj);
}

void AbstractPlugin::removeObject(QObject *obj)
{
    PluginManager::removeObject(obj);
}
