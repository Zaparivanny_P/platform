#ifndef ABSTRACTPLUGIN_H
#define ABSTRACTPLUGIN_H

#include <QObject>

class AbstractPlugin : public QObject
{
    Q_OBJECT
public:
    explicit AbstractPlugin(QObject *parent = 0);
    virtual bool initialize() = 0;
    void addAutoReleasedObject(QObject *obj);
    void removeObject(QObject *obj);
signals:

public slots:
};

#endif // ABSTRACTPLUGIN_H
