#include "pluginmanager.h"

#include <QDir>
#include <QPluginLoader>
#include <QApplication>

#include <QWidget>
#include <QList>

#include <QDebug>

QList<AbstractPlugin*> PluginManager::m_pluginsList = QList<AbstractPlugin*>();
QList<QObject*> PluginManager::m_allObject;
PluginManager *PluginManager::m_instance = nullptr;

PluginManager::PluginManager(QObject *parent) : QObject(parent)
{

}

PluginManager *PluginManager::instance()
{
    if(!m_instance)
    {
        m_instance = new PluginManager();
    }
    return m_instance;
}

void PluginManager::addPlugin(AbstractPlugin *plugin)
{
    m_pluginsList.append(plugin);
}

void PluginManager::loadPlugins()
{

}

void PluginManager::initialize()
{
    for(auto it : m_pluginsList)
    {
        it->initialize();
    }
}

void PluginManager::addObject(QObject *obj)
{
    m_allObject.append(obj);
    Q_ASSERT_X(m_instance != nullptr, "PluginManager::addObject", "instance is null");
    emit m_instance->objectAdded(obj);
}

void PluginManager::removeObject(QObject *obj)
{
    m_allObject.removeOne(obj);
}


