#-------------------------------------------------
#
# Project created by QtCreator 2016-09-18T22:21:24
#
#-------------------------------------------------

QT       += core gui 
QT += network
QT += serialport

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Platform
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    pluginmanager.cpp \
    abstractplugin.cpp \
    plugins/ProjectManager/projectmanager.cpp \
    plugins/ProjectManager/projectmanagertools.cpp \
    plugins/Simple/simple.cpp \
    plugins/Simple/simplesupertools.cpp \
    plugins/CoreTester/coretester.cpp \
    plugins/CoreTester/coretestfactory.cpp \
    plugins/CoreTester/imode.cpp \
    plugins/CoreTester/modemanager.cpp \
    plugins/CoreTester/tabwidget.cpp \
    plugins/CoreTester/itestmode.cpp \
    plugins/CoreTester/testmode.cpp \
    plugins/CoreTester/itestview.cpp \
    plugins/AutoTest/autotestplugin.cpp \
    plugins/AutoTest/autotestmainview.cpp \
    plugins/CoreTester/settingmode.cpp \
    plugins/CoreTester/isettingmode.cpp \
    plugins/AutoTest/autotestsettings.cpp \
    plugins/SlaveNetManager/slavenetmanager.cpp \
    plugins/SlaveNetManager/net/netclientreply.cpp \
    plugins/SlaveNetManager/net/netprotocol.cpp \
    plugins/SlaveNetManager/net/netslaveclient.cpp \
    plugins/SlaveNetManager/modbus/modbusreply.cpp \
    plugins/SlaveNetManager/modbus/modbusdataunit.cpp \
    plugins/SlaveNetManager/modbus/modbuspdu.cpp \
    plugins/SlaveNetManager/modbus/modbusclient.cpp \
    plugins/AutoTest/stagelistview.cpp \
    plugins/AutoTest/iautoteststage.cpp \
    plugins/AutoTest/controlpanel.cpp \
    plugins/AutoTestStage1/autoteststage1.cpp \
    plugins/AutoTestStage1/stageview.cpp \
    plugins/CoreTester/welcomemode.cpp

HEADERS  += mainwindow.h \
    pluginmanager.h \
    abstractplugin.h \
    plugins/ProjectManager/projectmanager.h \
    plugins/ProjectManager/projectmanagertools.h \
    plugins/Simple/simple.h \
    plugins/Simple/simplesupertools.h \
    plugins/CoreTester/coretester.h \
    plugins/CoreTester/coretestfactory.h \
    plugins/CoreTester/imode.h \
    plugins/CoreTester/modemanager.h \
    plugins/CoreTester/tabwidget.h \
    plugins/CoreTester/itestmode.h \
    plugins/CoreTester/testmode.h \
    plugins/CoreTester/itestview.h \
    plugins/AutoTest/autotestplugin.h \
    plugins/AutoTest/autotestmainview.h \
    plugins/CoreTester/settingmode.h \
    plugins/CoreTester/isettingmode.h \
    plugins/AutoTest/autotestsettings.h \
    plugins/SlaveNetManager/slavenetmanager.h \
    plugins/SlaveNetManager/net/netclientreply.h \
    plugins/SlaveNetManager/net/netprotocol.h \
    plugins/SlaveNetManager/net/netslaveclient.h \
    plugins/SlaveNetManager/modbus/modbusreply.h \
    plugins/SlaveNetManager/modbus/modbusdataunit.h \
    plugins/SlaveNetManager/modbus/modbuspdu.h \
    plugins/SlaveNetManager/modbus/modbusclient.h \
    plugins/AutoTest/stagelistview.h \
    plugins/AutoTest/iautoteststage.h \
    plugins/AutoTest/controlpanel.h \
    plugins/AutoTestStage1/autoteststage1.h \
    plugins/AutoTestStage1/stageview.h \
    plugins/CoreTester/welcomemode.h

RESOURCES += \
    resource.qrc
