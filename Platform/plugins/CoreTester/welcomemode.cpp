#include "welcomemode.h"
#include <QIcon>
#include <QWidget>
#include "pluginmanager.h"
#include "itestview.h"
#include <QGridLayout>
#include <QPushButton>
#include <QListWidget>


WelcomeMode::WelcomeMode(QObject *parent) : IMode(parent)
{
    connect(PluginManager::instance(), &PluginManager::objectAdded, this, &WelcomeMode::objectAdded);

    m_listWidget = new QListWidget;
    m_glay = new QHBoxLayout();
    m_glay->setMargin(0);
    m_glay->setSpacing(0);
    m_widget = new QWidget();
    m_widget->setLayout(m_glay);
    m_glay->addWidget(m_listWidget);

    m_listWidget->setViewMode(QListView::IconMode);
    m_listWidget->setFocusPolicy(Qt::NoFocus);
    m_listWidget->setMovement(QListView::Static);
    m_listWidget->setResizeMode(QListView::Adjust);

    m_listWidget->setWrapping(true);
    m_listWidget->setTextElideMode(Qt::ElideRight);
    m_listWidget->setUniformItemSizes(true);
    m_listWidget->setFrameShape(QFrame::NoFrame);
    m_listWidget->setIconSize(QSize(100, 100));
    m_listWidget->setSpacing(20);

}

QString WelcomeMode::name()
{
    return "Начало";
}

QIcon WelcomeMode::icon()
{
    return QIcon(":/icons/menu-6.png");
}

QWidget *WelcomeMode::widget()
{
    //m_widget->setStyleSheet("background-color:red;");
    m_widget->setMinimumSize(100, 100);
    return m_widget;
}

void WelcomeMode::objectAdded(QObject *obj)
{
    ITestView *view = qobject_cast<ITestView*>(obj);
    if(view)
    {
        for(int i = 0; i < 1; i++)
        {
            QListWidgetItem *item = new QListWidgetItem(view->icon(), view->name());
            m_listWidget->addItem(item);
        }
    }
}
