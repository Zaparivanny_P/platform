#include "coretester.h"
#include <QLayout>
#include <QWidget>
#include <QListWidget>
#include <QDebug>
#include <QSplitter>
#include <QPushButton>
#include <QApplication>

#include "pluginmanager.h"
#include "coretestfactory.h"
#include "tabwidget.h"
#include "testmode.h"
#include "welcomemode.h"
#include "settingmode.h"

CoreTester::CoreTester(QObject *parent) : AbstractPlugin(parent)
{
    m_glay = new QGridLayout;
    m_vlay = new QVBoxLayout;

    m_centralWidget = new QWidget;
   // m_centralWidget->setLayout(m_glay);
    
    
    QVBoxLayout * layBtn = new QVBoxLayout();
    m_tabWidget = new TabWidget();
    
    
    layBtn->addWidget(m_tabWidget);
    layBtn->setMargin(0);
    layBtn->setSpacing(0);
    m_centralWidget->setLayout(layBtn);
    

    WelcomeMode *welcome = new WelcomeMode();
    m_tabWidget->insertTab(0, welcome->widget(), welcome->icon(), welcome->name());

    TestMode *test = new TestMode("Тест");
    m_tabWidget->insertTab(1, test->widget(), test->icon(), test->name());
    
    SettingMode *setting = new SettingMode();
    m_tabWidget->insertTab(2, setting->widget(), setting->icon(), setting->name());
    
    /*QWidget *mainPanel = new QWidget;
    mainPanel->setStyleSheet("background-color:green;");
    mainPanel->setMinimumHeight(150);
    mainPanel->setLayout(m_vlay);
    
    QWidget *bottomPanel = new QWidget;
    bottomPanel->setStyleSheet("background-color:black;");
    bottomPanel->setMinimumHeight(150);
    
    QSplitter * splitter = new QSplitter(Qt::Vertical);
    
    splitter->addWidget(mainPanel);
    splitter->addWidget(bottomPanel);
    
    m_glay->addWidget(leftPanel, 0, 0, 100, 1);
    m_glay->addWidget(splitter, 0, 1, 100, 100);
    
    m_glay->setMargin(0);
    m_glay->setSpacing(0);
    
    m_vlay->setMargin(0);
    m_vlay->setSpacing(0);*/
}

bool CoreTester::initialize()
{
    connect(PluginManager::instance(), SIGNAL(objectAdded(QObject*)), SLOT(objectAdded(QObject*)));
    return true;
}

QWidget *CoreTester::widget()
{
    return m_centralWidget;
}

void CoreTester::objectAdded(QObject *obj)
{
    IMode *test = qobject_cast<IMode*>(obj);
    if(test)
    {
        m_tabWidget->insertTab(3, test->widget(), test->icon(), test->name());
    }
}
