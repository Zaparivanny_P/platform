#ifndef SETTINGMODE_H
#define SETTINGMODE_H

#include <QObject>
#include "imode.h"

#include <QTabWidget>

class SettingMode : public IMode
{
    Q_OBJECT
public:
    explicit SettingMode(QObject *parent = 0);
    virtual QString name() override;
    virtual QIcon icon() override;
    virtual QWidget *widget() override;
private:
    QTabWidget *m_tabWidget;
signals:
    
public slots:
    void objectAdded(QObject* obj);
};

#endif // SETTINGMODE_H
