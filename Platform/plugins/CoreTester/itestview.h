#ifndef ITESTVIEW_H
#define ITESTVIEW_H

#include <QObject>
class QWidget;

class ITestView : public QObject
{
    Q_OBJECT
public:
    ITestView(QObject *parent = 0);
    virtual QWidget *widget() = 0;
    virtual QString name() = 0;
    virtual QIcon icon() = 0;

};

#endif // ITESTVIEW_H
