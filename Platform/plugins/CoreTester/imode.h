#ifndef IMODE_H
#define IMODE_H

#include <QObject>

class IMode : public QObject
{
    Q_OBJECT
public:
    explicit IMode(QObject *parent = 0);
    virtual QString name() = 0;
    virtual QIcon icon() = 0;
    virtual QWidget *widget() = 0;
signals:
    
public slots:
};

#endif // IMODE_H
