#ifndef WELCOMEMODE_H
#define WELCOMEMODE_H

#include <QObject>
#include "imode.h"

class QHBoxLayout;
class QListWidget;

class WelcomeMode : public IMode
{
    Q_OBJECT
public:
    explicit WelcomeMode(QObject *parent = 0);

    QString name();
    QIcon icon();
    QWidget *widget();
private:
    QHBoxLayout *m_glay;
    QWidget *m_widget;
    QListWidget *m_listWidget;
signals:

public slots:
    void objectAdded(QObject *obj);
};

#endif // WELCOMEMODE_H
