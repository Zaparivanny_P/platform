#ifndef ISETTINGMODE_H
#define ISETTINGMODE_H

#include <QObject>

class QWidget;


class ISettingMode : public QObject
{
    Q_OBJECT
public:
    explicit ISettingMode(QObject *parent = 0);
    virtual QWidget *widget() = 0;
    virtual QString name() = 0;
signals:
    
public slots:
};

#endif // ISETTINGMODE_H
