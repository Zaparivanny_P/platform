#include "settingmode.h"

#include <QApplication>
#include <QStyle>
#include <QWidget>

#include "isettingmode.h"
#include "pluginmanager.h"

SettingMode::SettingMode(QObject *parent) : IMode(parent)
{
    connect(PluginManager::instance(), SIGNAL(objectAdded(QObject*)), SLOT(objectAdded(QObject*)));
    m_tabWidget = new QTabWidget();
}

QString SettingMode::name()
{
    return "Настойки";
}

QIcon SettingMode::icon()
{
    return QIcon(":/icons/settings-4.png");
}

QWidget *SettingMode::widget()
{
    //QWidget *widget = new QWidget;
    
    return m_tabWidget;
}

void SettingMode::objectAdded(QObject *obj)
{
    ISettingMode *setting = qobject_cast<ISettingMode*>(obj);
    if(setting)
    {
        m_tabWidget->addTab(setting->widget(), setting->name());
    }
}
