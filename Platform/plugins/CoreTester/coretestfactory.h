#ifndef CORETESTFACTORY_H
#define CORETESTFACTORY_H

#include <QObject>
class QIcon;

class CoreTestFactory : public QObject
{
    Q_OBJECT
public:
    explicit CoreTestFactory(QString name, QObject *parent = 0);
    virtual QWidget *widget() = 0;
    virtual QString name();
    virtual QIcon icon() = 0;
private:
    QString m_name;
signals:
    
public slots:
};

#endif // CORETESTFACTORY_H
