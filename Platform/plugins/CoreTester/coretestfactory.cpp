#include "coretestfactory.h"

CoreTestFactory::CoreTestFactory(QString name, QObject *parent) : QObject(parent), m_name(name)
{
    
}

QString CoreTestFactory::name()
{
    return m_name;
}
