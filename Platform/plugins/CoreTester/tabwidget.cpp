#include "tabwidget.h"

#include <QPushButton>
#include <QVBoxLayout>
#include <QLabel>
#include <QIcon>
#include <QStyle>
#include <QApplication>
#include <QStackedWidget>
#include <QListWidget>
#include <QDebug>

TabWidget::TabWidget(QWidget *parent) : QWidget(parent)
{
   
    m_list = new QListWidget(parent);
    m_list->setStyleSheet(
    "QListWidget"
    "{background: qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:0, stop:0 rgba(100, 100, 100, 255), stop:1 rgba(150, 150, 150, 255));"
    "border-right: 2px groove gray;}"
    "QListWidget::item:hover "
    "{background: qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:0, stop:0 rgba(150, 150, 150, 255), stop:1 rgba(200, 200, 200, 255)); }"
    "QListWidget::item:selected "
    "{background: qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:0, stop:0 rgba(200, 200, 200, 200), stop:1 rgba(220, 220, 220, 255)); }"
    "QListWidget::item "
    "{border-bottom: 2px groove gray; padding: 10px; color: white; }");
    
    
    m_list->setMaximumWidth(70);
    m_modesStack = new QStackedLayout(parent);
    m_currentWidget = new QWidget;
    m_currentWidget->setLayout(m_modesStack);
    
    m_list->setViewMode(QListView::IconMode);
    //m_list->setStyleSheet("font: bold 20px;");
    m_list->setFlow(QListView::TopToBottom);
    m_list->setFocusPolicy(Qt::NoFocus); 
    m_list->setMovement(QListView::Static);
    m_list->setResizeMode(QListView::Adjust);
    
    m_list->setWrapping(true);
    m_list->setTextElideMode(Qt::ElideRight);
    m_list->setUniformItemSizes(true);
    m_list->setFrameShape(QFrame::NoFrame);
    
    connect(m_list, SIGNAL(itemClicked(QListWidgetItem*)), SLOT(itemChanged(QListWidgetItem*)));
    
    
    QLayout *vlay = new QHBoxLayout();
    vlay->addWidget(m_list);
    vlay->addWidget(m_currentWidget);
    
    vlay->setMargin(0);
    vlay->setSpacing(0);
    
    setLayout(vlay);
}

void TabWidget::insertTab(int index, QWidget *tab, const QIcon &icon, const QString &label)
{
    QListWidgetItem *item = new QListWidgetItem(icon, label);
    item->setSizeHint(QSize(70, 70));
    m_list->insertItem(index, item);
    int row = m_list->row(item);
    
    m_modesStack->insertWidget(row, tab);
}

void TabWidget::removeTab(int index)
{
    m_modesStack->removeWidget(m_modesStack->widget(index));
    //m_tabBar->removeTab(index);
}

void TabWidget::setCurrentIndex(int index)
{
    m_modesStack->setCurrentIndex(index);
    //m_list->setCurrentIndex(index);
}

void TabWidget::itemChanged(QListWidgetItem *item)
{
    int index = m_list->row(item);
    m_modesStack->setCurrentIndex(index);
}
