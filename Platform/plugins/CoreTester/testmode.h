#ifndef TESTMODE_H
#define TESTMODE_H

#include <QObject>
#include "imode.h"


class QIcon;
class QWidget;
class QString;
class QLayout;
class QStackedLayout;

class TestMode : public IMode
{
    Q_OBJECT
public:
    explicit TestMode(QString name, QObject *parent = 0);
    
    virtual QString name() override;
    virtual QIcon icon() override;
    virtual QWidget *widget() override;
private:
    QString m_name;
    QWidget *m_widget;
    QLayout *m_hlay;
    QStackedLayout *m_stackLay;
signals:
    
public slots:
    void objectAdded(QObject* obj);

};

#endif // TESTMODE_H
