#include "testmode.h"
#include <QApplication>
#include <QStyle>
#include <QIcon>
#include <QWidget>
#include <QSplitter>
#include <QLayout>
#include <QStackedLayout>

#include <QDebug>

#include "itestview.h"
#include "pluginmanager.h"

TestMode::TestMode(QString name, QObject *parent) : IMode(parent), m_name(name)
{
    connect(PluginManager::instance(), SIGNAL(objectAdded(QObject*)), SLOT(objectAdded(QObject*)));
}

QString TestMode::name()
{
    return m_name;
}

QIcon TestMode::icon()
{
    return QIcon(":/icons/controls-8.png");
}

QWidget *TestMode::widget()
{
    //QWidget *mainPanel = new QWidget;
    //mainPanel->setStyleSheet("background-color:black;");
    
    m_widget = new QWidget;
    m_widget->setStyleSheet("background-color:green;");
    m_widget->setMinimumHeight(150);
    
    QWidget *bottomPanel = new QWidget;
    bottomPanel->setStyleSheet("background-color:black;");
    bottomPanel->setMinimumHeight(150);
    
    QSplitter * splitter = new QSplitter(Qt::Vertical);
    
    splitter->addWidget(m_widget);
    splitter->addWidget(bottomPanel);

    m_stackLay = new QStackedLayout();
    m_widget->setLayout(m_stackLay);

    return splitter;
}

void TestMode::objectAdded(QObject *obj)
{
    ITestView *view = qobject_cast<ITestView*>(obj);
    if(view)
    {
        m_stackLay->addWidget(view->widget());
    }
}
