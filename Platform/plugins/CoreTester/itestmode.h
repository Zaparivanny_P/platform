#ifndef ITESTMODE_H
#define ITESTMODE_H

#include <QObject>

class ITestMode : public QObject
{
    Q_OBJECT
public:
    explicit ITestMode(QObject *parent = 0);
    
signals:
    
public slots:
};

#endif // ITESTMODE_H