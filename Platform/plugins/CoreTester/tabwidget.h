#ifndef TABWIDGET_H
#define TABWIDGET_H

#include <QWidget>
#include <QStackedLayout>

class QListWidget;
class QListWidgetItem;

class TabWidget : public QWidget
{
    Q_OBJECT
public:
    explicit TabWidget(QWidget *parent = 0);
    void insertTab(int index, QWidget *tab, const QIcon &icon, const QString &label);
    void removeTab(int index);
private:
    QStackedLayout *m_modesStack;
    QWidget *m_currentWidget;
    QListWidget *m_list;
signals:
    void currentChanged(int index);
public slots:
    void setCurrentIndex(int index);
    void itemChanged(QListWidgetItem*);
};

#endif // TABWIDGET_H
