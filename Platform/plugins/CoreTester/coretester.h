#ifndef CORETESTER_H
#define CORETESTER_H

#include <QObject>
#include <QMap>
#include "abstractplugin.h"

class QWidget;
class QLayout;
class QListWidget;
class QGridLayout;
class TabWidget;

class CoreTester : public AbstractPlugin
{
    Q_OBJECT
public:
    explicit CoreTester(QObject *parent = 0);
    virtual bool initialize() override;
    QWidget* widget();
signals:
    
public slots:
    void objectAdded(QObject *obj);
private:
    QGridLayout *m_glay;
    QLayout *m_vlay;
    QWidget *m_centralWidget;
    QListWidget *m_listView;
    QMap<QString, QWidget*> m_tests;
    
    TabWidget *m_tabWidget;
};

#endif // CORETESTER_H
