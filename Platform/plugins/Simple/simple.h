#ifndef SIMPLE_H
#define SIMPLE_H

#include <QObject>
#include "abstractplugin.h"

class Simple : public AbstractPlugin
{
    Q_OBJECT
public:
    explicit Simple(QObject *parent = 0);
    bool initialize();
signals:

public slots:

};

#endif // SIMPLE_H
