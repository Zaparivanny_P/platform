#ifndef SIMPLESUPERTOOLS_H
#define SIMPLESUPERTOOLS_H

#include <QObject>
#include "plugins/CoreTester/coretestfactory.h"

class SimpleSuperTools : public CoreTestFactory
{
    Q_OBJECT
public:
    explicit SimpleSuperTools(QString name, QObject *parent = 0);
    virtual QWidget *widget();
    virtual QIcon icon() override;
signals:

public slots:
    
};

#endif // SIMPLESUPERTOOLS_H
