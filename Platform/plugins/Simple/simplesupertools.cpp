#include "simplesupertools.h"
#include <QPushButton>
#include <QVBoxLayout>
#include <QLabel>
#include <QIcon>
#include <QStyle>
#include <QApplication>
#include <QStackedWidget>
#include <QListWidget>

SimpleSuperTools::SimpleSuperTools(QString name, QObject *parent) : CoreTestFactory(name, parent)
{

}

QWidget *SimpleSuperTools::widget()
{
    
    QWidget *mainPanel = new QWidget;
    mainPanel->setStyleSheet("background-color:black;");

    return mainPanel;
}

QIcon SimpleSuperTools::icon()
{
    return QApplication::style()->standardIcon(QStyle::SP_MessageBoxWarning);
}
