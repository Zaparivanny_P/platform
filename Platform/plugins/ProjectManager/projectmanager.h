#ifndef PROJECTMANAGER_H
#define PROJECTMANAGER_H

#include <QObject>
#include "abstractplugin.h"

class QWidget;
class QLayout;

class ProjectManager : public AbstractPlugin
{
    Q_OBJECT
public:
    explicit ProjectManager(QObject *parent = 0);
    bool initialize();
    QWidget* widget();
signals:

public slots:
    void objectAdded(QObject *obj);
private:
    QLayout *m_vlay;

};

#endif // PROJECTMANAGER_H
