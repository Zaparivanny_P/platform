#include "projectmanager.h"
#include <QDebug>

#include <QWidget>
#include <QLayout>
#include <QPushButton>

#include "projectmanagertools.h"
#include "pluginmanager.h"

ProjectManager::ProjectManager(QObject *parent) : AbstractPlugin(parent)
{
    m_vlay = new QVBoxLayout();
}

bool ProjectManager::initialize()
{
    connect(PluginManager::instance(), SIGNAL(objectAdded(QObject*)), SLOT(objectAdded(QObject*)));
    qDebug() << "ProjectManager::initialize";
    return true;
}

QWidget *ProjectManager::widget()
{
    QWidget *widget = new QWidget();

    m_vlay->addWidget(new QPushButton("Plugin manager"));
    widget->setLayout(m_vlay);
    return widget;
}

void ProjectManager::objectAdded(QObject *obj)
{
    qDebug() << "[ProjectManager::objectAdded]";
    ProjectManagerTools *tools = qobject_cast<ProjectManagerTools*>(obj);
    if(tools)
    {
        //qDebug() << tools->widget();
        m_vlay->addWidget(tools->widget());
    }
}
