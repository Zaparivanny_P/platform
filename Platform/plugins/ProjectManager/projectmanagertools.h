#ifndef PROJECTMANAGERTOOLS_H
#define PROJECTMANAGERTOOLS_H

#include <QObject>
class QWidget;

class ProjectManagerTools : public QObject
{
    Q_OBJECT
public:
    explicit ProjectManagerTools(QObject *parent = 0);
    virtual QWidget *widget() = 0;
signals:

public slots:
};

#endif // PROJECTMANAGERTOOLS_H
