#include "slavenetmanager.h"
#include "net/netclientreply.h"
#include "net/netprotocol.h"
#include "net/netslaveclient.h"

#include "modbus/modbusclient.h"
#include "modbus/modbuspdu.h"
#include "modbus/modbusreply.h"

NetSlaveClient *SlaveNetManager::m_client = nullptr;

SlaveNetManager::SlaveNetManager(QObject *parent) : AbstractPlugin(parent)
{

}

bool SlaveNetManager::initialize()
{
    
    m_client = new NetSlaveClient();
    
    //NetConnection *cmodel = new NetConnection;
    //NetProtocol *m_protocol = new NetProtocol(m_client, cmodel);
    
    //m_client->connectToHost("127.0.0.1", 45454);
    
    /*ModbusClient client;
    client.setNetClient(m_client);
    ModbusRequest request(ModbusRequest::ReadInputRegisters,QByteArray::fromHex("00020002"));
    ModbusReply *reply = client.sendRawRequest(request, 0x1);
    connect(reply, &ModbusReply::finished, [this, reply](){
        qDebug() << "SlaveNetManager::initialize()" << reply->rawResult().data();
        
        qDebug() << "SlaveNetManager::initialize()" << reply->result().isValid();
        qDebug() << "SlaveNetManager::initialize()" << reply->result().value(2);
        qDebug() << "SlaveNetManager::initialize()" << reply->result().value(3);
        reply->deleteLater();
    });*/
    
    /*ModbusClient client;
    ModbusReply *reply;
    client.setNetClient(m_client);
    
    ModbusDataUnit write(ModbusDataUnit::HoldingRegisters);
    write.setStartAddress(7);
    write.setValueCount(2);
    QVector<quint16> vec = {30, 1};
    write.setValues(vec);
    reply = client.sendWriteRequest(write, 4);
    connect(reply, &ModbusReply::finished, [this, reply](){
        qDebug() << "SlaveNetManager::initialize()" << reply->result().isValid();
        qDebug() << "SlaveNetManager::initialize()" << reply->result().values();
        qDebug() << "SlaveNetManager::initialize()" << reply->rawResult().isException();
        reply->deleteLater();
    });
    
    
    ModbusDataUnit dataunit(ModbusDataUnit::InputRegisters);
    dataunit.setStartAddress(7);
    dataunit.setValueCount(4);
    reply = client.sendReadRequest(dataunit, 4);

    connect(reply, &ModbusReply::finished, [this, reply](){
        qDebug() << "SlaveNetManager::initialize()" << reply->result().isValid();
        qDebug() << "SlaveNetManager::initialize()" << reply->result().values();
        qDebug() << "SlaveNetManager::initialize()" << reply->rawResult().isException();
        reply->deleteLater();
    });*/
    
    return true;
}

NetSlaveClient *SlaveNetManager::client()
{
    return m_client;
}
