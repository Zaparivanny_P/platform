#include "netclientreply.h"

NetClientReply::NetClientReply(QObject *parent) : QObject(parent)
{
    
}

void NetClientReply::setFinished()
{
    emit finished();
}

quint8 NetClientReply::error1() const
{
    return m_error1;
}

void NetClientReply::setError1(const quint8 &error1)
{
    m_error1 = error1;
}

quint8 NetClientReply::error2() const
{
    return m_error2;
}

void NetClientReply::setError2(const quint8 &error2)
{
    m_error2 = error2;
}

QByteArray NetClientReply::data() const
{
    return m_data;
}

void NetClientReply::setData(const QByteArray &data)
{
    m_data = data;
}

quint8 NetClientReply::cmd() const
{
    return m_cmd;
}

void NetClientReply::setCmd(const quint8 &cmd)
{
    m_cmd = cmd;
}
