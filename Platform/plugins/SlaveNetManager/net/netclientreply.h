#ifndef NETCLIENTREPLY_H
#define NETCLIENTREPLY_H

#include <QObject>

class NetClientReply : public QObject
{
    Q_OBJECT
public:
    explicit NetClientReply(QObject *parent = 0);
    
    void setFinished();
    
    quint8 error1() const;
    void setError1(const quint8 &error1);
    
    quint8 error2() const;
    void setError2(const quint8 &error2);
    
    QByteArray data() const;
    void setData(const QByteArray &data);
    
    quint8 cmd() const;
    void setCmd(const quint8 &cmd);
    
private:
    quint8 m_error1;
    quint8 m_error2;
    QByteArray m_data;
    quint8 m_cmd;
signals:
    void finished();
public slots:
};

#endif // NETCLIENTREPLY_H
