#ifndef NETPROTOCOL_H
#define NETPROTOCOL_H

#include <QObject>
#include <QDataStream>
#include <QDebug>
//#include "netprotocoldata.h"
#include "netslaveclient.h"
#include "netclientreply.h"

#include <QSerialPort>
#include <QSerialPortInfo>
#include <QDataStream>

class NetConnection
{
public:
    QSerialPort::BaudRate baudrate() const {return m_baudrate;}
    void setBaudrate(const QSerialPort::BaudRate &baudrate) {m_baudrate = baudrate;}
    
    QSerialPort::StopBits stopBits() const {return m_stopBits;}
    void setStopBits(const QSerialPort::StopBits &stopBits) {m_stopBits = stopBits;}
    
    QSerialPort::Parity parity() const {return m_parity;}
    void setParity(const QSerialPort::Parity &parity){m_parity = parity;}
    
    QSerialPort::FlowControl flowControl() const{return m_flowControl;}
    void setFlowControl(const QSerialPort::FlowControl &flowControl){m_flowControl = flowControl;}
    
    QSerialPort::DataBits dataBits() const{return m_dataBits;}
    void setDataBits(const QSerialPort::DataBits &dataBits){m_dataBits = dataBits;}
    
private:
    QSerialPort::BaudRate m_baudrate;
    QSerialPort::DataBits m_dataBits;
    QSerialPort::StopBits m_stopBits;
    QSerialPort::Parity m_parity;
    QSerialPort::FlowControl m_flowControl;
};

//QDebug operator<<(QDebug debug, const ConnectionModel &model);
QDataStream &operator<<(QDataStream &stream, const NetConnection &model);

class NetProtocol : public QObject
{
    Q_OBJECT
public:
    NetProtocol(NetSlaveClient *netSlaveClient, NetConnection *model, QObject *parent = 0);
    
    quint16 number() const;
    void setNumber(const quint16 &number);
    
    QString portName() const;
    void setPortName(const QString &portName);
    
    QMap<QString, QString> key() const;
    void setKey(const QMap<QString, QString> &key);
    
    NetClientReply* sendArray(const QByteArray data, 
                              const QMap<QString, QString> key = QMap<QString, QString>());
    NetClientReply* sendArray(const QString &portName, const QByteArray data,
                              const QMap<QString, QString> key = QMap<QString, QString>());
    NetClientReply* sendArray(const QString &portName, const NetConnection &cmodel, const QByteArray data,
                              const QMap<QString, QString> key = QMap<QString, QString>());
    NetClientReply* sendModusPdu(const quint8 address, const QByteArray data, 
                                 const QMap<QString, QString> key = QMap<QString, QString>());
    
    NetClientReply* availablePorts(const QMap<QString, QString> key = QMap<QString, QString>());
    
private:
    quint16 getNumber();
private:
    quint8 m_protocol;
    quint16 m_number;
    QString m_portName;
    QMap<QString, QString> m_key;
    NetSlaveClient *m_netSlaveClient;
    NetConnection *m_connectionModel;
    QMap<quint16, NetClientReply*> m_mapReply;
    //NetProtocolData m_data;
private slots:
    void receive(QByteArray data);
};

QDebug operator<<(QDebug debug, const NetProtocol &pdu);
QDataStream &operator<<(QDataStream &stream, const NetProtocol &pdu);

#endif // NETPROTOCOL_H
