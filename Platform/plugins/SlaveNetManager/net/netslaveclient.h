#ifndef NETSLAVECLIENT_H
#define NETSLAVECLIENT_H

#include <QObject>
#include <QTcpSocket>

class NetSlaveClient : public QObject
{
    Q_OBJECT
public:
    explicit NetSlaveClient(QObject *parent = 0);
    void connectToHost(QString ip, int port);
    void disconnectFromHost();
    void write(QByteArray data);
private:
    void decode(QByteArray data);
private:
    QTcpSocket socket;
    quint16 blockSize = 0;
signals:
    void error(QString err);
    void receive(QByteArray data);
public slots:
    void displayError(QAbstractSocket::SocketError socketError);
    void readyRead();
};

#endif // NETSLAVECLIENT_H
