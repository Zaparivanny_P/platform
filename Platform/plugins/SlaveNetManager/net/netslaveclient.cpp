#include "netslaveclient.h"
#include <QDataStream>
#include <QSerialPort>

NetSlaveClient::NetSlaveClient(QObject *parent) : QObject(parent), socket(this)
{
    connect(&socket, SIGNAL(error(QAbstractSocket::SocketError)), 
            SLOT(displayError(QAbstractSocket::SocketError)));
    connect(&socket, SIGNAL(readyRead()), SLOT(readyRead()));
}

void NetSlaveClient::connectToHost(QString ip, int port)
{
    socket.connectToHost(ip, port);
}

void NetSlaveClient::disconnectFromHost()
{
    socket.disconnectFromHost();
}

void NetSlaveClient::write(QByteArray data)
{
    QByteArray block;
    QDataStream out(&block, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_5_6);
    out << (quint16)0;
    out << (quint32)1;
    out << (quint32)0;
    out << data;
    out.device()->seek(0);
    out << (quint16)(block.size() - sizeof(quint16));
    
    socket.waitForBytesWritten();
    quint64 cnt = socket.write(block);
    //qDebug() << "[NetSlaveClient::write] size:" << block.size() << cnt;
}

void NetSlaveClient::decode(QByteArray data)
{
    QByteArray result;
    QDataStream stream(&data, QIODevice::ReadOnly);
    
    quint32 blockNums, blockCnt; 
    stream >> blockNums >> blockCnt >> result;

    //qDebug() << "[NetSlaveClient]" << result.toHex();
    emit receive(result);
}

void NetSlaveClient::displayError(QAbstractSocket::SocketError socketError)
{
    QString err;
    switch (socketError) 
    {
    case QAbstractSocket::RemoteHostClosedError:
        break;
    case QAbstractSocket::HostNotFoundError:
        err = QString(tr("The host was not found. Please check the "
                                    "host name and port settings."));
        break;
    case QAbstractSocket::ConnectionRefusedError:
        err = QString(tr("The connection was refused by the peer. "
                                    "Make sure the fortune server is running, "
                                    "and check that the host name and port "
                                    "settings are correct."));
        break;
    default:
        err = QString(tr("The following error occurred: %1.")
                                 .arg(socket.errorString()));
    }
    emit error(err);
}

void NetSlaveClient::readyRead()
{
    QDataStream stream(&socket);
    while(socket.bytesAvailable() > 1)
    {
    
        if(blockSize == 0)
        {
            stream >> blockSize;
        }
        
        if(socket.bytesAvailable() < blockSize)
        {
            return;
        }
        
        
        QByteArray data = socket.read(blockSize);
        blockSize = 0;
        int numRead = 0;
        numRead = data.size();
    
        if (numRead == 0)
        {
            qDebug() << "receive empty package";
            return;
        }
        //qDebug() << "[NetSlaveClient::readyRead] decode";
        decode(data);
    }

}
