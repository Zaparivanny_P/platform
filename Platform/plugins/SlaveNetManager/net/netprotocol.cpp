#include "netprotocol.h"

NetProtocol::NetProtocol(NetSlaveClient *netSlaveClient, NetConnection *model, QObject *parent) :
    m_netSlaveClient(netSlaveClient),
    m_connectionModel(model),
    QObject(parent)
{
    connect(m_netSlaveClient, SIGNAL(receive(QByteArray)), this, SLOT(receive(QByteArray)));
}

quint16 NetProtocol::number() const
{
    return m_number;
}

void NetProtocol::setNumber(const quint16 &number)
{
    m_number = number;
}

QString NetProtocol::portName() const
{
    return m_portName;
}

void NetProtocol::setPortName(const QString &portName)
{
    m_portName = portName;
}

QMap<QString, QString> NetProtocol::key() const
{
    return m_key;
}

void NetProtocol::setKey(const QMap<QString, QString> &key)
{
    m_key = key;
}

NetClientReply *NetProtocol::sendArray(const QByteArray data, const QMap<QString, QString> key)
{
    return sendArray(m_portName, *m_connectionModel, data, key);
}

NetClientReply *NetProtocol::sendArray(const QString &portName, 
                                       const QByteArray data, 
                                       const QMap<QString, QString> key)
{
    return sendArray(portName, *m_connectionModel, data, key);
}

NetClientReply *NetProtocol::sendArray(const QString &portName, 
                                       const NetConnection &cmodel, 
                                       const QByteArray data, 
                                       const QMap<QString, QString> key)
{
    QByteArray result;
    QDataStream stream(&result, QIODevice::WriteOnly);
    quint16 num = getNumber();
    stream << (quint8)1 << (quint16)num << portName << cmodel << key << data;
    m_netSlaveClient->write(result);
    NetClientReply *reply = new NetClientReply;
    m_mapReply.insert(num, reply);
    return reply;
}



NetClientReply *NetProtocol::sendModusPdu(const quint8 address, 
                                          const QByteArray data, 
                                          const QMap<QString, QString> key)
{
    QByteArray adu;
    QDataStream streamAdu(&adu, QIODevice::WriteOnly);
    streamAdu << address;
    streamAdu << data;
    
    QByteArray result;
    QDataStream stream(&result, QIODevice::WriteOnly);
    quint16 num = getNumber();
    stream << (quint8)2 << (quint16)num << m_portName << *m_connectionModel << key << adu;
    m_netSlaveClient->write(result);
    NetClientReply *reply = new NetClientReply;
    m_mapReply.insert(num, reply);
    return reply;
}

NetClientReply *NetProtocol::availablePorts(const QMap<QString, QString> key)
{
    QByteArray result;
    QDataStream stream(&result, QIODevice::WriteOnly);
    quint16 num = getNumber();
    stream << (quint8)4 << (quint16)num << key;
    m_netSlaveClient->write(result);
    NetClientReply *reply = new NetClientReply;
    m_mapReply.insert(num, reply);
    return reply;
}

quint16 NetProtocol::getNumber()
{
    static quint16 cnt = 0;
    cnt++;
    
    do
    {
        if(m_mapReply.find(cnt) == m_mapReply.end())
        {
            break;
        }   
    }while(1);
    
    return cnt;
}

void NetProtocol::receive(QByteArray data)
{
    //qDebug() << "[NetProtocol]" << data.toHex();
    
    quint8 e1, e2, cmd;
    quint16 number;
    QByteArray rdata;
    QMap<QString, QString> mapValue;
    QDataStream inb(&data, QIODevice::ReadOnly);
    inb >> cmd >> number >> e1 >> e2 >> rdata >> mapValue;
    
    /*qDebug() << "[NetProtocol::receive]" << "cmd:" << cmd;
    qDebug() << "[NetProtocol::receive]" << "number:"<< number;
    qDebug() << "[NetProtocol::receive]" << "error1:" << e1;
    qDebug() << "[NetProtocol::receive]" << "error2:" << e2;
    qDebug() << "[NetProtocol::receive]" << "data:" << rdata.toHex();*/
    
    NetClientReply *reply = m_mapReply.value(number);
    Q_ASSERT_X(reply != NULL, "NetProtocol::receive(QByteArray data)", "reply is null");

    reply->setCmd(cmd);
    reply->setError1(e1);
    reply->setError2(e2);
    
    if(!rdata.isEmpty())
    {
        reply->setData(rdata);
    }
    
    reply->setFinished();
    //delete reply;
    m_mapReply.remove(number);
}

QDebug operator<<(QDebug debug, const NetProtocol &pdu)
{
    debug <<(quint16)pdu.number() << pdu.portName() << pdu.key();
    return debug;
}
    
QDataStream &operator<<(QDataStream &stream, const NetProtocol &pdu)
{
    stream <<(quint16)pdu.number();
    stream << pdu.key();
    return stream;
}


QDataStream &operator<<(QDataStream &stream, const NetConnection &model)
{
    QByteArray data;
    QDataStream out(&data, QIODevice::WriteOnly);
    out << (quint8)model.parity() << (quint32)model.baudrate() << (quint8)model.dataBits() 
           << (quint8)model.stopBits() << (quint16)1000;
    stream << data;
    return stream;
}
