#ifndef SLAVENETMANAGER_H
#define SLAVENETMANAGER_H

#include <QObject>
#include <abstractplugin.h>

class NetSlaveClient;

class SlaveNetManager : public AbstractPlugin
{
    Q_OBJECT
public:
    explicit SlaveNetManager(QObject *parent = 0);
    virtual bool initialize() override;
    static NetSlaveClient* client();
private:
    static NetSlaveClient *m_client;
signals:
    
public slots:
};

#endif // SLAVENETMANAGER_H
