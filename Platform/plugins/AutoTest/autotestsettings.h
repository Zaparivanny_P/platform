#ifndef AUTOTESTSETTINGS_H
#define AUTOTESTSETTINGS_H

#include <QObject>
#include "../CoreTester/isettingmode.h"

class AutoTestSettings : public ISettingMode
{
    Q_OBJECT
public:
    explicit AutoTestSettings(QObject *parent = 0);
    virtual QWidget *widget() override;
    virtual QString name() override;
signals:
    
public slots:
    
};

#endif // AUTOTESTSETTINGS_H
