#ifndef AUTOTESTMAINVIEW_H
#define AUTOTESTMAINVIEW_H

#include <QObject>
#include "../CoreTester/itestview.h"

class StageListView;

class AutoTestMainView : public ITestView
{
    Q_OBJECT
public:
    explicit AutoTestMainView(QObject *parent = 0);
    virtual QWidget *widget() override;
    QString name();
    QIcon icon();
private:
    StageListView *m_stageList;
signals:
    
public slots:
    void objectAdded(QObject* obj);
};

#endif // AUTOTESTMAINVIEW_H
