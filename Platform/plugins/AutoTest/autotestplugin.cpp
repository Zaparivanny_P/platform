#include "autotestplugin.h"
#include "autotestmainview.h"
#include "autotestsettings.h"

AutoTestPlugin::AutoTestPlugin(QObject *parent) : AbstractPlugin(parent)
{
    
}

bool AutoTestPlugin::initialize()
{
    addAutoReleasedObject(new AutoTestMainView());
    addAutoReleasedObject(new AutoTestSettings());
    return true;
}
