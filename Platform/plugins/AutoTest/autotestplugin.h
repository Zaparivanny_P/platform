#ifndef AUTOTESTPLUGIN_H
#define AUTOTESTPLUGIN_H

#include <QObject>
#include "abstractplugin.h"

class AutoTestPlugin : public AbstractPlugin
{
    Q_OBJECT
public:
    explicit AutoTestPlugin(QObject *parent = 0);
    virtual bool initialize() override;
signals:
    
public slots:
    
};

#endif // AUTOTESTPLUGIN_H
