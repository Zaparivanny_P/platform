#include "autotestmainview.h"
#include <QWidget>
#include <QIcon>
#include <QApplication>
#include <QStyle>

#include "stagelistview.h"
#include "controlpanel.h"
#include "iautoteststage.h"

#include "pluginmanager.h"

AutoTestMainView::AutoTestMainView(QObject *parent) : ITestView(parent)
{
    connect(PluginManager::instance(), SIGNAL(objectAdded(QObject*)), SLOT(objectAdded(QObject*)));
}

QWidget *AutoTestMainView::widget()
{
    QWidget *w = new QWidget();

    m_stageList = new StageListView();
    
    ControlPanel *cpanel = new ControlPanel();
    cpanel->setMaximumHeight(60);
    cpanel->setMinimumHeight(60);
    cpanel->setStyleSheet("background-color:red;");
    QLayout *vlay = new QVBoxLayout();
    vlay->addWidget(m_stageList);
    vlay->addWidget(cpanel);
    vlay->setMargin(0);
    vlay->setSpacing(0);
    
    w->setLayout(vlay);
    return w;
}

QString AutoTestMainView::name()
{
    return "Simple test";
}

QIcon AutoTestMainView::icon()
{
    return QIcon(":/icons/route.png");
}

void AutoTestMainView::objectAdded(QObject *obj)
{
    IAutoTestStage *stage = qobject_cast<IAutoTestStage*>(obj);
    if(stage)
    {
        m_stageList->insertTab(m_stageList->size(), stage->widget(), stage->icon(), stage->name());
    }
}
