#ifndef PLUGINMANAGER_H
#define PLUGINMANAGER_H

#include <QObject>
#include "abstractplugin.h"

class PluginManager : public QObject
{
    Q_OBJECT
public:
    static PluginManager *instance();
    static void addPlugin(AbstractPlugin *plugin);
    static void loadPlugins();
    static void initialize();

    static void addObject(QObject *obj);
    static void removeObject(QObject *obj);
protected:
    explicit PluginManager(QObject *parent = 0);
private:
    static QList<AbstractPlugin*> m_pluginsList;
    static QList<QObject*> m_allObject;
    static PluginManager *m_instance;
signals:
    void objectAdded(QObject *obj);
    void initializationDone();
};

#endif // PLUGINMANAGER_H
