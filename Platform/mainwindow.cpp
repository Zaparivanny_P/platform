#include "mainwindow.h"

#include "pluginmanager.h"

#include "plugins/ProjectManager/projectmanager.h"
#include "plugins/Simple/simple.h"
#include "plugins/CoreTester/coretester.h"
#include "plugins/AutoTest/autotestplugin.h"
#include "plugins/SlaveNetManager/slavenetmanager.h"
#include "plugins/AutoTestStage1/autoteststage1.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    
    CoreTester *pm = new CoreTester();
    PluginManager::addPlugin(pm);
    //PluginManager::addPlugin(new Simple());
    PluginManager::addPlugin(new AutoTestPlugin());
    PluginManager::addPlugin(new AutoTestStage1());
    
    PluginManager::addPlugin(new SlaveNetManager());
    
    PluginManager::initialize();
    setMinimumSize(1024, 768);
    setCentralWidget(pm->widget());
}

MainWindow::~MainWindow()
{

}
